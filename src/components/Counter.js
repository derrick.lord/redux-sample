import React, {Component} from 'react';
import { connect } from 'react-redux';

import '../css/components/Counter.css';
import logo from '../images/logo.svg';

class Counter extends Component{
	increment = ()=>{
		this.props.dispatch({type: 'INCREMENT'});
	}

	decrement = ()=>{
		this.props.dispatch({type: 'DECREMENT'});
	}

	setRotation = ()=> {
		if(this.props.count >= 5 && this.props.count < 10){
			return 'Counter-logo-2x';
		}
		if(this.props.count >= 10 && this.props.count < 20){
			return 'Counter-logo-4x ';
		}
		if(this.props.count < 0 && this.props.count > -5){
			return 'Counter-logo-reverse';
		}
		if(this.props.count <= -5 && this.props.count > -10){
			return 'Counter-logo-reverse-2x';
		}
		if(this.props.count <= -10 && this.props.count > -20){
			return 'Counter-logo-reverse-4x';
		}
		return 'Counter-logo'
	}

	render(){
		return (
			<div className="counter">
				<h2>Counter</h2>
				<div>
					<img src={logo} className={this.setRotation()} alt="logo" />
					<button className="btn btn-primary" onClick={this.decrement}>-</button>
					<span className="count">{this.props.count}</span>
					<button className="btn btn-primary" onClick={this.increment}>+</button>
				</div>
			</div>
		);
	}

}

const mapStateToProps = (state) => ({
	count: state.count
});

export default connect(mapStateToProps)(Counter);