import React from 'react';

// Redux setup
import { Provider } from 'react-redux';
import store from './store/counterStore';

// Import Counter Component
import Counter from './components/Counter';

// Layout Styling 
import 'bootstrap/dist/css/bootstrap.min.css';
import './css/App.css';
import logo from './images/logo.svg';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
      </header>
      <Provider store={store}>
        <Counter/>
      </Provider>
    </div>
  );
}

export default App;
